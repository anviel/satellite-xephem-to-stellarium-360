# Satellite XEphem to Stellarium 360

A utility software for exporting Earth satellite definitions from XEphem into Stellarium 360.

## Description
This command line utility:
- reads an XEphem satellite database file (usually downloaded from NORAD website) or by default the file located at _.xephem/stations.edb_
- get the orbital elements of the desired Earth-orbiting satellite (by default this is the International Space Station)
- convert the orbital elements from equatorial to ecliptic coordinates
- reads a Stellarium 360 _ssystem.ini_ configuration file
- if an entry for the desired satellite already exists, it updates its orbital elements
- if the entry does not exist yet, it is appended to the end of the configuration file

## Usage
<code>sat_xephem_stell360.pl \[-x \<path to stations.edb\>\] \[-s \<path to ssystem.ini\>\] \<name of satellite in Xephem\> \<name of satellite in Stellarium\></code>

If -x \<path to stations.edb\> is omitted, it takes _$HOME/.xephem/stations.edb_

If -s \<path to ssystem.ini\> is omitted, it takes _$HOME/.stellarium360/ssystem.ini_

If \<name of satellite in Xephem\> is missing, it takes _ISS (ZARYA)_

If \<name of satellite in Stellarium\> is missing, it takes _iss_.

## Example of use
> `$ ./sat_xephem_stell360.pl`  
> `orbital elements of body ISS (ZARYA) read from /home/user/.xephem/stations.edb`  
> `epoch = 2023-03-06.5619`  
> `inclination/equator = 51.640701 deg`  
> `right ascension of ascending node = 116.8871 deg`  
> `eccentricity = 0.00058250001`  
> `argument of perigee = 62.860802 deg`  
> `mean motion = 15.49751744 rev/day`  
> `mean anomaly @ epoch = 85.463699 deg`  
> 
> `orbital elements computed:`  
> `epoch (JD) = 2460010.06188042`  
> `period = 0.0645264639237599 day`  
> `semi-major axis = 6793.75447975319 km`  
> `inclination/ecliptic = 64.634958904914 deg`  
> `longitude of ascending node = 129.287031614961 deg`  
> `longitude of pericenter = 169.035597124992 deg`  
> `mean longitude @ epoch = 254.499296124992 deg`  
> 
> `process /home/user/.stellarium360/ssystem.ini`  
> `iss found`  
> `update orbit_meanlongitude`  
> `update orbit_epoch`  
> `update orbit_ascendingnode`  
> `update orbit_inclination`  
> `update orbit_period`  
> `update orbit_semimajoraxis`  
> `update orbit_eccentricity`  
> `update orbit_longofpericenter`  


## Requirements
- [XEphem](https://github.com/XEphem/XEphem) 
- [Stellarium 360](http://www.lss-planetariums.info/index.php?lang=en&menu=stellarium360&page=stellarium360)
- Perl 5
- gfortran
- swig

## Installation
Simply enter the _src_ directory and type _make_.

It builds the Fortran module needed for the astronomical computations, and generates the Perl/Fortran wrapper using swig.

The main Perl script _sat_xephem_stell360.pl_ is ready to use.
