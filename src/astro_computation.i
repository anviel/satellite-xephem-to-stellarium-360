%module astro_computation
%include "typemaps.i"
%{
void date_to_jd_(double *, int *, int *, double *);
void equatorial_to_ecliptic_(double *, double *, double *, double *, double *, double *);
void mean_motion_to_sma_(double *, double *);
%}
void date_to_jd_(double *INPUT, int* INPUT, int* INPUT, double* OUTPUT);
void equatorial_to_ecliptic_(double* INPUT, double* INPUT, double* INPUT, double* OUTPUT, double* OUTPUT, double* OUTPUT);
void mean_motion_to_sma_(double* INPUT, double* OUTPUT);
