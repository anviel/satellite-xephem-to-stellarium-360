#!/usr/bin/perl -I.
# ------------------------------------------------------------------------------
# File       : sat_xephem_stell360.pl
# Purpose    : Read the orbital elements of a satellite from XEphem database
#              file, then convert from equatorial coordinates to
#              ecliptic coordinates and store into the stellarium 360
#              configuration file
# Author     : A. Viel, Club d'Astronomie Jupiter du Roannais
# Created on : 2023-03-05
# Modified on: 2023-03-08
# ------------------------------------------------------------------------------
use strict;
#use Math::Trig;
#use POSIX;
use Getopt::Std;
use astro_computation;

# ----------------------------------------
#
#        File manipulation functions
#
# ----------------------------------------

#
# Read the ssystem.ini file and look for a body
#
sub read_ssystem
{
   my $filename = shift; # path to the ssystem.ini file
   my $bdyname = shift;  # body name

   my @flines;
   my $found = 0;
   open(my $fo, "<", $filename) or die $filename." not found!";
   while(not eof($fo))
   {
      my $a_line = <$fo>;
      chomp $a_line;
      push @flines, $a_line; 
      $found = 1 if ($a_line eq "[".$bdyname."]");
   }
   close($fo);
   return ($found, @flines);
} # read_ssystem

#
# Rewrite the ssystem.ini file,
# and replacing the orbital elements of a body
#
sub replace_ssystem
{
   my $filename = shift;
   my $bdyname  = shift;  # body name

   # the dictionary of replaceable element keywords
   my $element_dict_ref = shift;
   my %element_dict = %{$element_dict_ref};

   # all the lines previously read in the file
   my $flines_ref = shift;
   my @flines = @{$flines_ref};

   # flag to tell if we are inside the body definition
   my $in_body = 0;

   open(my $fo, ">", $filename);
   foreach my $a_line (@flines)
   {
      # by default, just copy the current line
      my $w_line = $a_line;
      # Look for the body
      if ($a_line eq "[".$bdyname."]")
      {
         $in_body = 1;
         print $fo $w_line, "\n";
         print $bdyname, " found\n";
         next;
      }
      if ($in_body == 1)
      {
         # Look for all the replaceable keywords
         foreach my $kw (keys %element_dict)
         {
            if ($a_line =~ /$kw/)
            {
               # replace
               $w_line = $kw . " = " . $element_dict{$kw};
               print "update ", $kw, "\n";
               last;  # stop here
            } 
         }
         # stop processing at next body
         $in_body = 0 if $a_line =~ /\[[a-z]*\]/;
      }
      # rewrite current line
      print $fo $w_line, "\n";
   }
   close($fo);
} # replace_ssystem

#
# Rewrite the ssystem.ini file,
# and add the new body definition at the end
#
sub append_ssystem
{
   my $filename = shift;
   my $bdyname  = shift;

   # the dictionary of replaceable element keywords
   my $element_dict_ref = shift;
   my %element_dict = %{$element_dict_ref};

   # all the lines previously read in the file
   my $flines_ref = shift;
   my @flines = @{$flines_ref};

   open(my $fo, ">", $filename);
   # just copy all the lines except the last
   foreach my $a_line (@flines[0 .. $#flines-1])
   {
      print $fo $a_line, "\n";
   }

   # create the body
   print $fo <<EOF;

[$bdyname]
name = $bdyname
parent = Earth
radius = 1
type = Satellite
halo = true
color = 1.,1.,1.
label_color = 0.5,0.5,0.5
orbit_color = 0.5,0.5,0.5
tex_map = bodies/generic.png
tex_halo = star16x16.png
coord_func = ell_orbit
lighting = false
albedo = 0.7
rot_periode = 24
rot_rotation_offset = 0
rot_obliquity = 0
rot_equator_ascending_node = 0
orbit_visualization_period = 0.06453025170682242
orbit_bounding_radius = .0027
tex_normal = bodies/nomap.png
EOF

   # now write all the replaceable keywords
   foreach my $kw (keys %element_dict)
   {
      # replace
      my $w_line = $kw . " = " . $element_dict{$kw};
      print $fo $w_line, "\n";
   }
   
   # last line
   print $fo $flines[$#flines], "\n";
   close($fo);
} # append_ssystem


# ----------------------------------------
#
#              Main program
#
# ----------------------------------------

# Check the command line arguments
my %opts;
getopts("hx:s:", \%opts);

if (exists $opts{h})
{
   print "usage: ", $0, " [-x stations.edb] [-s ssystem.ini]\n          [<body in xephem>] [<body in stellarium>]\n";
   exit 0;
}

# Look for the input and output data files
my $infilename = $ENV{"HOME"} . "/.xephem/stations.edb";
$infilename = $opts{x} if exists $opts{x};
my $outfilename = $ENV{"HOME"}. "/.stellarium360/ssystem.ini";
$outfilename = $opts{s} if exists $opts{s};

# Get the name of the satellite/body to convert
my $xephem_body_name = $ARGV[0];
my $stellarium_body_name = $ARGV[1];
$xephem_body_name = "ISS (ZARYA)" if $xephem_body_name eq "";
$stellarium_body_name = "iss" if $stellarium_body_name  eq "";

# Look for the "stations" file donwloaded from NORAD
if (! -r $infilename)
{
   print $infilename, " does not exist!\nOpen XEphem > Data > Download...\n";
   exit(0);
}

# Read the file and look for the body
my $bfound = 0;
my @fields;
open(my $fh, "<", $infilename);
while(not eof($fh))
{
   my $a_line = <$fh>;
   @fields = split /,/, $a_line;
   if ($fields[0] eq $xephem_body_name)
   {
      #print $a_line;
      $bfound = 1;
      last;
   }
}
close($fh);
if ($bfound == 0)
{
   printf("body %s not found in %s\n", $xephem_body_name, $infilename);
   exit 1;
}

# Get the epoch month/day.frac/year,
# to be converted to Julian Day Number later
my @subfields = split /\|/, $fields[2];
my @dmy   = split /\//, $subfields[0];
my $day   = eval $dmy[1];
my $month = eval $dmy[0];
my $year  = eval $dmy[2];

# Now process the orbital element fields
my $inclination_eq = eval $fields[3]; 
my $raan           = eval $fields[4];
my $eccentricity   = eval $fields[5];
my $argperi        = eval $fields[6];
my $mean_anomaly   = eval $fields[7];
my $n1             = eval $fields[8];  # mean motion

# Print the read quantities
printf("orbital elements of body %s read from %s\n",
       $xephem_body_name, $infilename);
printf("epoch = %4d-%02d-%07.4f\n", $year, $month, $day);
print "inclination/equator = ", $inclination_eq, " deg\n";
print "right ascension of ascending node = ", $raan, " deg\n";
print "eccentricity = ", $eccentricity, "\n";
print "argument of perigee = ", $argperi, " deg\n";
print "mean motion = ", $n1, " rev/day\n";
print "mean anomaly @ epoch = ", $mean_anomaly, " deg\n";

# Compute output quantities
my $epoch = astro_computation::date_to_jd_($day, $month, $year);
my $period = 1.0 / $n1;
my $sma = astro_computation::mean_motion_to_sma_($n1);
my ($inclination_ecl, $lan, $lon_peri) =
   astro_computation::equatorial_to_ecliptic_($inclination_eq, $raan, $argperi);
my $mean_longitude = $mean_anomaly + $lon_peri;
$mean_longitude -= 360 if $mean_longitude > 360;

# Print computed quantities
print "\norbital elements computed:\n";
print "epoch (JD) = ", $epoch, "\n";
print "period = ", $period, " day\n";
print "semi-major axis = ", $sma, " km\n";
print "inclination/ecliptic = ", $inclination_ecl, " deg\n";
print "longitude of ascending node = ", $lan, " deg\n";
print "longitude of pericenter = ", $lon_peri, " deg\n";
print "mean longitude @ epoch = ", $mean_longitude, " deg\n";

# Store data into a dictionary
my %element_dict = (
   orbit_period =>  $period,
   orbit_epoch => $epoch,
   orbit_semimajoraxis => $sma,
   orbit_eccentricity => $eccentricity,
   orbit_inclination => $inclination_ecl,
   orbit_ascendingnode => $lan,
   orbit_longofpericenter => $lon_peri,
   orbit_meanlongitude => $mean_longitude
);

# Read the output file and look for satellite
my ($found, @flines) = read_ssystem($outfilename, $stellarium_body_name);
print "\nprocess ", $outfilename, "\n";
if ($found == 1)
{
   replace_ssystem($outfilename, $stellarium_body_name,
                   \%element_dict, \@flines);
}
else
{
   print $stellarium_body_name, " not found: append it to file\n";
   append_ssystem($outfilename, $stellarium_body_name, \%element_dict, \@flines);
}
