! ------------------------------------------------------------------------------
! File       : astro_computation.f95
! Purpose    : Various subroutines for computing astronomical quantities
! Author     : A. Viel, Club d'Astronomie Jupiter du Roannais
! Created on : 2023-03-06
! Modified on: 2023-03-07
! ------------------------------------------------------------------------------
subroutine date_to_jd(day, month, year, jdn)
! Convert a Gregorian date into a Julian Day Number.
!
! Parameters:
! -----------
! day  : the (possibly fractional) date between 1 and 31
! month: the month number between 1 and 12
! year : the four digit year
!
! Return:
! -------
! jdn  : the Julian day number
implicit none
double precision, intent(in) :: day
integer, intent(in) :: month, year
double precision, intent(out) :: jdn
integer :: y, m, a, b, c, d
y = year
m = month
if (month .eq. 1 .or. month .eq. 2) then
   y = y - 1
   m = m + 12
endif
a = y / 100
b = 2 - a + a / 4
c = floor(365.25 * y)
d = floor(30.6001 * (m+1))
jdn = b + c + d + day + 1720994.5;
end subroutine

subroutine rot_mat_X(t, r)
! Build the rotation matrix around X axis
!
! Parameters:
! -----------
! t: the rotation angle in degrees
!
! Return:
! -------
! r: the 3x3 rotation matrix
implicit none
double precision, intent(in) :: t
double precision, dimension(3,3), intent(out) :: r
double precision, parameter :: pi = 3.1415926536
double precision :: tr  ! angle in rad
double precision :: costr, sintr
tr = t * pi / 18D1
costr = cos(tr)
sintr = sin(tr)
r = 0D0  ! all zeros
r(1, 1) = 1D0
r(2, 2) = costr
r(3, 2) = sintr
r(2, 3) = -sintr
r(3, 3) = costr
end subroutine

subroutine rot_mat_Y(t, r)
! Build the rotation matrix around Y axis
!
! Parameters:
! -----------
! t: the rotation angle in degrees
!
! Return:
! -------
! r: the 3x3 rotation matrix
implicit none
double precision, intent(in) :: t
double precision, dimension(3,3), intent(out) :: r
double precision, parameter :: pi = 3.1415926536
double precision :: tr  ! angle in rad
double precision :: costr, sintr
tr = t * pi / 18D1
costr = cos(tr)
sintr = sin(tr)
r = 0D0  ! all zeros
r(1, 1) = costr
r(3, 1) = -sintr
r(2, 2) = 1D0
r(1, 3) = sintr
r(3, 3) = costr
end subroutine

subroutine rot_mat_Z(t, r)
! Build the rotation matrix around Z axis
!
! Parameters:
! -----------
! t: the rotation angle in degrees
!
! Return:
! -------
! r: the 3x3 rotation matrix
implicit none
double precision, intent(in) :: t
double precision, dimension(3,3), intent(out) :: r
double precision, parameter :: pi = 3.1415926536
double precision :: tr  ! angle in rad
double precision :: costr, sintr
tr = t * pi / 18D1
costr = cos(tr)
sintr = sin(tr)
r = 0D0  ! all zeros
r(1, 1) = costr
r(2, 1) = sintr
r(1, 2) = -sintr
r(2, 2) = costr
r(3, 3) = 1D0
end subroutine

subroutine cross_product(u, v, r)
! Vector (or cross) product of two vectors
!
!          r = u x v
!
! Parameters:
! -----------
! u: a 3-element vector
! v: a 3-element vector
!
! Return:
! -------
! r: a 3-element vector, the result
implicit none
double precision, dimension(3), intent(in)  :: u, v
double precision, dimension(3), intent(out) :: r
r(1) = u(2)*v(3) - u(3)*v(2)
r(2) = u(3)*v(1) - u(1)*v(3)
r(3) = u(1)*v(2) - u(2)*v(1)
end subroutine

subroutine equatorial_to_ecliptic(incli_eq, raan, arg_peri, &
                                  incli_ecl, lan, long_peri)
! Convert orbital elements from equatorial to ecliptic coordinate framework
!
! Parameters:
! -----------
! incli_eq : inclination over celestial equator, in degree
! raan     : right ascension of ascending node, in degree
! arg_peri : argument of pericenter, in degree
!
! Return:
! -------
! incli_ecl: inclination over ecliptic plane, in degree
! lan      : longitude of ascending node, in degree
! long_peri: longitude of pericenter, in degree
implicit none
double precision, intent(in)  :: incli_eq, raan, arg_peri
double precision, intent(out) :: incli_ecl, lan, long_peri
! constants
double precision, parameter :: obliquity = 23.433333333333334  ! deg
double precision, parameter :: pi = 3.1415926536
! local var.
double precision, dimension(3) :: v1, v2, vz = (/ 0D0, 0D0, 1D0 /), &
                                  xloc, yloc, zloc, &
                                  periapsis, periapsis_loc
double precision, dimension(3, 3) :: rz, rx, rorb, reclip, reclipt
! rotation matrix from orbital to equatorial
call rot_mat_Z(raan, rz)
call rot_mat_X(incli_eq, rx)
rorb = matmul(rz, rx)
! normal vector to orbital plane
v1 = matmul(rorb, vz)
! rotation matrix from ecliptic to equatorial
call rot_mat_X(obliquity, reclip)
reclipt = transpose(reclip)
! normal vector to ecliptic plane
v2 = matmul(reclip, vz)
! inclination over ecliptic
incli_ecl = 18D1/pi * acos(dot_product(v1, v2))
if (abs(incli_ecl) .lt. 1.666D-2) then
   ! inclination is less than 1 arcmin
   lan = 0D0
   long_peri = arg_peri
else
   ! normal to orbital plane in ecliptic coords
   zloc = matmul(reclipt, v1)
   ! longitude of ascending node
   lan = 18D1/pi * atan2(zloc(1), -zloc(2))
   if (lan .lt. 0D0) then
      lan = lan + 36D1
   endif
   ! periapsis in orbital plane coords
   periapsis = (/ cos(pi/18D1*arg_peri), &
                  sin(pi/18D1*arg_peri), &
                  0D0 /)
   ! convert to equatorial coords
   periapsis = matmul(rorb, periapsis)
   ! convert now to ecliptic coords
   periapsis = matmul(reclipt, periapsis)
   ! build the local coord frame
   xloc = (/ -zloc(2), zloc(1), 0D0 /)
   xloc = xloc / norm2(xloc)
   call cross_product(zloc, xloc, yloc)
   ! get the coords of periapsis in orbital plane
   periapsis_loc(1) = dot_product(xloc, periapsis)
   periapsis_loc(2) = dot_product(yloc, periapsis)
   !periapsis_loc(3) = dot_product(zloc, periapsis) ! should be 0
   ! finally get the local argument of pericenter
   ! and add the longitude of ascending node to get the
   ! longitude of pericenter
   long_peri = lan + 18D1/pi * atan2(periapsis_loc(2), periapsis_loc(1))
   if (long_peri .gt. 36D1) then
      long_peri = long_peri - 36D1
   endif
endif
end subroutine

subroutine mean_motion_to_sma(n, sma)
! Compute the semi-major axis (a) of an earth-orbiting satellite
! from its mean motion (n) using Kepler's second law:
!
!           n^2 a^3 = GM
!
! Parameters:
! -----------
! n  : the mean motion, expressed in rev/day
!
! Return:
! -------
! sma: the semi-major axis, expressed in km
implicit none
double precision, intent(in)  :: n   ! mean motion, rev/day
double precision, intent(out) :: sma ! semi-major axis, km
double precision, parameter :: pi = 3.1415926536
double precision, parameter :: GM = 3.984658D14  ! earth gravitational constant
double precision, parameter :: solar_day = 86400 ! solar day, second
double precision :: n2 ! square of mean motion, rad^2/s^2
n2  = (2 * pi * n / solar_day) ** 2
sma = 1D-3 * (GM / n2) ** 3.3333D-1
end subroutine
